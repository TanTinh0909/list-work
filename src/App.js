import React, { Component } from "react";
import Product from "./components/Product";
import ColorPicker from "./components/ColorPicker";
import Reset from "./components/Reset";
import SizeSeting from "./components/SizeSeting";
import Result from "./components/Result";
import FormReactjs from "./components/FormReactjs";
import "./App.css";
import TaskForm from "./components/TaskForm";
import MultipleSort from "./components/MultipleSort";
import TaskList from "./components/TaskList";
class App extends Component {
  constructor(props) {
    super(props);
    // this.onAddItem = this.onAddItem.bind(this);
    this.state = {
      item: [
        {
          id: 1,
          name: "phan tan tinh",
          status: true,
          age: 20,
          city: "quang ngai city",
          img:
            "https://znews-photo.zadn.vn/w660/Uploaded/qhj_yvobvhfwbv/2018_07_18/Nguyen_Huy_Binh1.jpg",
        },
        {
          id: 2,
          name: "phan tan tinh",
          status: true,
          age: 20,
          city: "quang ngai city",
          img:
            "https://znews-photo.zadn.vn/w660/Uploaded/qhj_yvobvhfwbv/2018_07_18/Nguyen_Huy_Binh1.jpg",
        },
        {
          id: 3,
          name: "phan tan tinh",
          status: true,
          age: 20,
          city: "quang ngai city",
          img:
            "https://znews-photo.zadn.vn/w660/Uploaded/qhj_yvobvhfwbv/2018_07_18/Nguyen_Huy_Binh1.jpg",
        },
        {
          id: 4,
          name: "phan tan tinh",
          status: true,
          age: 20,
          city: "quang ngai city",
          img:
            "https://znews-photo.zadn.vn/w660/Uploaded/qhj_yvobvhfwbv/2018_07_18/Nguyen_Huy_Binh1.jpg",
        },
        {
          id: 5,
          name: "phan tan tinh",
          status: true,
          age: 20,
          city: "quang ngai city",
          img:
            "https://znews-photo.zadn.vn/w660/Uploaded/qhj_yvobvhfwbv/2018_07_18/Nguyen_Huy_Binh1.jpg",
        },
      ],
      isActivate: true,
      color: "red",
      fontSize: 15,
      task: [],
      isDisplayForm: false,
      tasking: null,
      filter: {
        name: "",
        status: -1,
      },
      keyword: "",
      sortBy: "name",
      sortValue: 1,
    };
  }
  // lifecycle
  componentWillMount() {
    if (localStorage && localStorage.getItem("task")) {
      let tasks = JSON.parse(localStorage.getItem("task"));
      this.setState({
        task: tasks,
      });
    }
  }
  s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  generateID = () => {
    return (
      this.s4() +
      this.s4() +
      "-" +
      this.s4() +
      this.s4() +
      "-" +
      this.s4() +
      this.s4() +
      "-" +
      this.s4() +
      this.s4() +
      "-" +
      this.s4() +
      this.s4()
    );
  };
  onsetColor = (setColor) => {
    this.setState({
      color: setColor,
    });
  };
  onTongleForm = () => {
    if (this.state.isDisplayForm && this.state.taskting !== null) {
      this.setState({
        isDisplayForm: true,
        taskting: null,
      });
    } else {
      this.setState({
        isDisplayForm: !this.state.isDisplayForm,
        taskting: null,
      });
    }
  };
  onCloseForm = () => {
    this.setState({
      isDisplayForm: false,
      taskting: null,
    });
  };
  onReceiveTask = (data) => {
    let { task } = this.state;
    if (data.id === undefined || data.id === "") {
      // add
      data.id = this.generateID();
      task.push(data);
    } else {
      // edit
      let index = this.findIndex(data.id);
      task[index] = data;
    }
    this.setState({
      task: task,
      taskting: null,
    });
    localStorage.setItem("task", JSON.stringify(task));
  };
  onUpdateStatus = (id) => {
    let { task } = this.state;
    let index = this.findIndex(id);
    if (index !== -1) {
      task[index].status = !task[index].status;
      this.setState({
        task: task,
      });
      localStorage.setItem("task", JSON.stringify(task));
    }
  };
  findIndex = (id) => {
    let { task } = this.state;
    let result = -1;
    task.forEach((element, index) => {
      if (element.id === id) {
        result = index;
      }
    });
    return result;
  };
  onDeleteTask = (id) => {
    let { task } = this.state;
    let index = this.findIndex(id);
    if (index !== -1) {
      task.splice(index, 1);
      this.setState({
        task: task,
      });
      localStorage.setItem("task", JSON.stringify(task));
    }
    this.onCloseForm();
  };
  onShowForm = () => {
    this.setState({
      isDisplayForm: true,
    });
  };
  onUpdateTask = (id) => {
    this.onShowForm();
    let { task } = this.state;
    let index = this.findIndex(id);
    let tasking = task[index];
    this.setState({
      taskting: tasking,
    });
  };
  onFilter = (filtername, filterstatus) => {
    filterstatus = parseInt(filterstatus);
    this.setState({
      filter: {
        name: filtername.toLowerCase(),
        status: filterstatus,
      },
    });
  };
  keywordsearch = (keyword) => {
    this.setState({
      keyword: keyword,
    });
  };
  onSort = (sortBy, sortValue) => {
    this.setState({
      sortBy: sortBy,
      sortValue: sortValue,
    });
  };
  onsetSize = (setSize) => {
    this.setState({
      fontSize: this.state.fontSize + setSize,
    });
  };
  onReset = (reset) => {
    if (reset) {
      this.setState({
        color: "red",
        fontSize: 15,
      });
    }
  };
  onAddItem = () => {
    console.log(this.refs.textInput.value);
  };
  onSetState = () => {
    this.setState({
      isActivate: !this.state.isActivate,
    });
  };

  render() {
    let {
      task,
      isDisplayForm,
      taskting,
      filter,
      keyword,
      sortBy,
      sortValue,
    } = this.state;
    console.log(sortValue);
    if (filter) {
      if (filter.name) {
        task = task.filter((task) => {
          return task.name.toLowerCase().indexOf(filter.name) !== -1;
        });
      }
      task = task.filter((task) => {
        if (filter.status === -1) {
          return task;
        } else {
          return task.status === (filter.status === 1 ? true : false);
        }
      });
      if (keyword) {
        task = task.filter((task) => {
          return task.name.toLowerCase().indexOf(keyword) !== -1;
        });
      }
    }
    if (sortBy === "name") {
      task.sort((a, b) => {
        if (a.name > b.name) return sortValue;
        else if(a.name < b.name) return -sortValue;
        return 0;
      });
    }else{
      task.sort((a, b) => {
        if (a.name > b.name) return sortValue;
        else if(a.name < b.name) return -sortValue;
        return 0;
      });
    }
    

    let elementDisplayForm = isDisplayForm ? (
      <TaskForm
        onCloseForm={this.onCloseForm}
        onReceiveTask={this.onReceiveTask}
        taskting={taskting}
      />
    ) : (
      ""
    );
    let elements = this.state.item.map((item, index) => {
      let result = "";
      if (item.status) {
        result = (
          <Product
            key={index}
            id={item.id}
            name={item.name}
            age={item.age}
            city={item.city}
            img={item.img}
          />
        );
      }
      return result;
    });
    return (
      <div className="">
        <div className="container">
          <div className="text-center">
            <h1>Quản Lý Công Việc</h1>
            <hr />
          </div>
          <div className="row">
            <div
              className={
                isDisplayForm === true
                  ? "col-xs-4 col-sm-4 col-md-4 col-lg-4"
                  : ""
              }
            >
              {/* Form */}
              {elementDisplayForm}
            </div>
            <div
              className={
                isDisplayForm === true
                  ? "col-xs-8 col-sm-8 col-md-8 col-lg-8"
                  : "col-xs-12 col-sm-12 col-md-12 col-lg-12"
              }
            >
              <button
                type="button"
                className="btn btn-primary"
                onClick={this.onTongleForm}
              >
                <span className="fa fa-plus mr-5" />
                Thêm Công Việc
              </button>
              {/* search  --- sort */}
              <MultipleSort
                keywordsearch={this.keywordsearch}
                onSort={this.onSort}
                sortBy={sortBy}
                sortValue={sortValue}
              />
              <div className="row mt-15">
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <TaskList
                    task={task}
                    onUpdateStatus={this.onUpdateStatus}
                    onDeleteTask={this.onDeleteTask}
                    onUpdateTask={this.onUpdateTask}
                    onFilter={this.onFilter}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <FormReactjs />
        <div className="container">
          <div className="row">
            <ColorPicker
              color={this.state.color}
              onReceiveColor={this.onsetColor}
            />
            <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <SizeSeting
                setSize={this.state.fontSize}
                onReceiveSize={this.onsetSize}
              />
              <Reset onReceiveReset={this.onReset} />
            </div>
            <Result color={this.state.color} setSize={this.state.fontSize} />
          </div>
        </div>
        <div className="col-md-12">
          <div className="form-group">
            <label>Tên sản phẩm</label>
            <input type="text" className="form-control" ref="textInput" />
          </div>
          <button className="btn btn-primary" onClick={this.onAddItem}>
            {" "}
            Lưu
          </button>
        </div>
        <div className="App">
          {this.state.isActivate ? elements : "no item elements"}
        </div>
        <button
          className={
            this.state.isActivate === true
              ? "btn btn-dark-green"
              : "btn btn-elegant"
          }
          onClick={this.onSetState}
        >
          activate {this.state.isActivate === true ? "true" : "false"}
        </button>
      </div>
    );
  }
}
export default App;
