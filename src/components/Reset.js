import React, { Component } from "react";

class Reset extends Component {

    getReset = () =>{
        this.props.onReceiveReset(true);
    }

    render() {
        return (
           <div className='App-reset'>
               <button className='btn btn-danger' onClick={this.getReset}>Reset</button>
           </div>
        );
    }
}
export default Reset;
