import React, { Component } from "react";

class ColorPicker extends Component {

    constructor(props) {
        super(props)
        this.state = {
            color: ['red', 'blue', 'green', '#ccc'],
        };
    }
    showColor = (color) => {
        return { backgroundColor: color}
    }
    setActiveColor = (color) => {
        this.props.onReceiveColor(color);
    }
    render() {
        let eleColor = this.state.color.map((color, index) => {
            return <span 
            className={this.props.color === color ? 'active': 'not-active'} 
            key={index} 
            onClick={()=>{this.setActiveColor(color)}}
            style={this.showColor(color)} ></span>
        })
        return (
            <div className='col-xs-6 col-sm-6 col-md-6 col-lg-6 app-ColorPicker'>
                <div className='ColorPicker'><span>ColorPicker</span></div>
                <div className='panel-body'>
                    {eleColor}
                </div>
            </div>
        );
    }
}
export default ColorPicker;
