import React, { Component } from "react";

class SizeSeting extends Component {

    setSize = (value) =>{
        this.props.onReceiveSize(value);
    }

    render() {
        return (
            <div className='app-ColorPicker'>
            <div className='ColorPicker'><span>Size {this.props.setSize}px </span>
            <button className='btn btn-success' onClick={()=>{this.setSize(1)}}>Tăng</button>
            <button className='btn btn-info' onClick={()=>{this.setSize(-1)}}>Giảm</button>
            </div>
        </div>
        );
    }
}
export default SizeSeting;
