import React, { Component } from "react";
class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyword: ''
    }
  }
  onChangeSearch = (event) => {
    let target = event.target;
    let value = target.value;
    let name = target.name;
    this.setState({
      [name]: value
    })
  }
  keywordSearch = () => {
    this.props.keywordsearch(this.state.keyword);
  }
  
  render() {
    let {keyword} = this.state;
    return (
        <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div className="input-group">
            <input
              type="text"
              className="form-control custom-search"
              placeholder="Nhập từ khóa..."
              name="keyword"
              value={keyword}
              onChange={this.onChangeSearch}
            />
            <span className="input-group-btn">
              <button className="btn btn-primary" type="button" onClick={this.keywordSearch}>
                <span className="fa fa-search mr-5" />
                Tìm
              </button>
            </span>
          </div>
        </div>
    );
  }
}

export default Search;
