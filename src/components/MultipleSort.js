import React, { Component } from "react";
import Search from "./Search";
import Sort from "./Sort";
class MultipleSort extends Component {
  render() {
    return (
      <div className="row mt-15">
        {/* search */}
        <Search keywordsearch={this.props.keywordsearch} />
        {/* sort */}
        <Sort
          onSort={this.props.onSort}
          sortBy={this.props.sortBy}
          sortValue={this.props.sortValue}
        />
      </div>
    );
  }
}

export default MultipleSort;
