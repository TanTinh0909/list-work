import React, { Component } from "react";

class Product extends Component {

    // constructor(props){
    //     super(props)
    //     this.onAddItem = this.onAddItem.bind(this);
    // }
    onAddItem = () =>{
        console.log(this.props.id);
    }
    render() {
        return (
            <div className="col-xs-3 col-ms-3 col-md-3 col-lg-3 item-product">
                <img alt={this.props.name} src={this.props.img} width="100%" />
                <h1> {this.props.name} </h1>
                <span>{this.props.age}</span>
                <h2>{this.props.city}</h2>
                <span className='btn btn-primary' onClick={this.onAddItem}> Mua ngay</span>
            </div>
        );
    }
}
export default Product;
