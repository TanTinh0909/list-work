import React, { Component } from "react";
import CheckBox from "./Checkbox";
class FormReactjs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txtUsername: "",
      txtPassword: "",
      txtMota: "",
      sltGender: 1,
      txtFramwork: "ts",
      fruites: [
        { id: 1, value: "banana", isChecked: true },
        { id: 2, value: "apple", isChecked: false },
        { id: 3, value: "mango", isChecked: false },
        { id: 4, value: "grap", isChecked: false },
      ],
    };
  }
  onHandleChange = (event) => {
    let target = event.target;
    let name = target.name;
    let value = target.value;
    this.setState({
      [name]: value,
    });
  };
  handleCheckChieldElement = (event) => {
    let fruites = this.state.fruites;
    fruites.forEach((fruite) => {
      if (fruite.value === event.target.value)
        fruite.isChecked = event.target.checked;
    });
    this.setState({ fruites: fruites });
  };
  onHandleSubmit = (event) => {
    event.preventDefault();
    console.log(this.state);
  };
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <div className="panel panel-primary app-panel">
              <div className="panel-heading set-panel">
                <h3 className="panel-title">Panel title</h3>
              </div>
              <div className="panel-body">
                <form className="custom-form" onSubmit={this.onHandleSubmit}>
                  <div className="form-group">
                    <label className="custom-lable">Username</label>
                    <input
                      type="text"
                      name="txtUsername"
                      className="form-control"
                      onChange={this.onHandleChange}
                      value={this.state.txtUsername}
                    />
                    <br />
                    <input
                      type="password"
                      name="txtPassword"
                      className="form-control"
                      onChange={this.onHandleChange}
                      value={this.state.txtPassword}
                    />
                  </div>
                  <div className="form-group">
                    <label>Mô tả</label>
                    <textarea
                      rows="3"
                      name="txtMota"
                      type="text"
                      className="form-control"
                      onChange={this.onHandleChange}
                      value={this.state.txtMota}
                    />
                  </div>
                  <label>giới tính</label>
                  <select
                    value={this.state.sltGender}
                    onChange={this.onHandleChange}
                    className="form-control"
                    name="sltGender"
                  >
                    <option value={0}>Nữ</option>
                    <option value={1}>Nam</option>
                  </select>
                  <br />

                  <div className="radio custom-flex">
                    <label>
                      <input
                        type="radio"
                        name="txtFramwork"
                        value="js"
                        onChange={this.onHandleChange}
                        checked={this.state.txtFramwork === "js"}
                      />
                      Reactjs
                    </label>
                    <label>
                      <input
                        type="radio"
                        name="txtFramwork"
                        value="ts"
                        onChange={this.onHandleChange}
                        checked={this.state.txtFramwork === "ts"}
                      />
                      Angular
                    </label>
                  </div>
                  <br />

                  <div className="checkbox custom-flex">
                    <ul>
                      {this.state.fruites.map((fruite, index) => {
                        return (
                          <CheckBox
                            key={index}
                            handleCheckChieldElement={
                              this.handleCheckChieldElement
                            }
                            {...fruite}
                          />
                        );
                      })}
                    </ul>
                  </div>

                  <button type="submit" className="btn btn-primary">
                    Lưu lại
                  </button>
                  <button type="reset" className="btn btn-blue-grey">
                    Xóa trắng
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default FormReactjs;
