import React, { Component } from "react";
import TaskItem from "./TaskItem";
class TaskList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filtername: "",
      filterstatus: -1,
    };
  }
  onChangeTask = (event) => {
    let target = event.target;
    let name = target.name;
    let value = target.value;
    this.props.onFilter(
      name === "filtername" ? value : this.state.filtername,
      name === "filterstatus" ? value : this.state.filterstatus
    );
    this.setState({
      [name]: value,
    });
  };

  render() {
    let { filtername, filterstatus } = this.state;
    let { task } = this.props;
    let elementTask = task.map((item, index) => {
      return (
        <TaskItem
          key={item.id}
          index={index}
          task={item}
          onUpdateStatus={this.props.onUpdateStatus}
          onDeleteTask={this.props.onDeleteTask}
          onUpdateTask={this.props.onUpdateTask}
        />
      );
    });
    return (
      <table className="table table-bordered table-hover">
        <thead>
          <tr>
            <th className="text-center">STT</th>
            <th className="text-center">Tên</th>
            <th className="text-center">Trạng Thái</th>
            <th className="text-center">Hành Động</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td />
            <td>
              <input
                type="text"
                className="form-control"
                name="filtername"
                value={filtername}
                onChange={this.onChangeTask}
              />
            </td>
            <td>
              <select
                className="form-control"
                name="filterstatus"
                value={filterstatus}
                onChange={this.onChangeTask}
              >
                <option value={-1}>Tất Cả</option>
                <option value={0}>Ẩn</option>
                <option value={1}>Kích Hoạt</option>
              </select>
            </td>
            <td />
          </tr>
          {elementTask}
        </tbody>
      </table>
    );
  }
}

export default TaskList;
