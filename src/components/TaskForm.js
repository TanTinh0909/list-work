import React, { Component } from "react";
class TaskForm extends Component {
  constructor(props) {
    super(props);
    this.state = { name: "", status: false };
  }
  onChange = (event) => {
    let target = event.target;
    let value = target.value;
    let name = target.name;
    if (name === "status") {
      value = target.value === "true" ? true : false;
    }
    this.setState({
      [name]: value,
    });
  };
  onHandleSubmit = (event) => {
    event.preventDefault();
    this.props.onReceiveTask(this.state);
    this.OnClear();
    this.onCloseForm();
  };
  OnClear = () => {
    this.setState({
      id: "",
      name: "",
      status: false,
    });
  };
  onCloseForm = () => {
    this.props.onCloseForm();
  };
  componentDidMount = () => {
    if (this.props.taskting) {
      this.setState({
        id: this.props.taskting.id,
        name: this.props.taskting.name,
        status: this.props.taskting.status,
      });
    }
  };
  componentWillReceiveProps = (nextProps) => {
    if (nextProps && nextProps.taskting) {
      this.setState({
        id: nextProps.taskting.id,
        name: nextProps.taskting.name,
        status: nextProps.taskting.status,
      });
    } else if (!nextProps.taskting) {
      this.setState({
        id: "",
        name: "",
        status: false,
      });
    }
  };
  render() {
    return (
      <div className="panel panel-warning panel-taskform">
        <div className="panel-heading panel-heading-taskform">
          <h3 className="panel-title panel-title-task">
            {this.state.name !== "" ? "Cập nhật công việc" : "Thêm công việc"}
          </h3>
          <span
            onClick={this.onCloseForm}
            className="fa fa-times-circle icon-close-task"
          ></span>
        </div>
        <div className="panel-body">
          <form className="custom-form-js" onSubmit={this.onHandleSubmit}>
            <div className="form-group">
              <label>Tên :</label>
              <input
                type="text"
                className="form-control"
                name="name"
                value={this.state.name}
                onChange={this.onChange}
              />
            </div>
            <label>Trạng Thái :</label>
            <select
              className="form-control"
              required="required"
              name="status"
              value={this.state.status}
              onChange={this.onChange}
            >
              <option value={true}>Kích Hoạt</option>
              <option value={false}>Ẩn</option>
            </select>
            <br />
            <div className="text-center">
              <button type="submit" className="btn btn-warning">
                Thêm
              </button>
              &nbsp;
              <button
                onClick={this.OnClear}
                type="button"
                className="btn btn-danger"
              >
                Hủy Bỏ
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default TaskForm;
