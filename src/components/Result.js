import React, { Component } from "react";

class Result extends Component {

    setStyle = () => {
        return {
            color: this.props.color,
            borderColor: this.props.color,
            fontSize: this.props.setSize,
        };
    }
    render() {
        return (
           <div className='App-result col-md-12'>
               <div className=''>Color: {this.props.color} Fontsize: {this.props.setSize}px</div>
               <div className='note-seting' style={this.setStyle()}> Nội dung {this.props.color}</div>
           </div>
        );
    }
}
export default Result;
