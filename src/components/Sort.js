import React, { Component } from "react";
import {
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
} from "mdbreact";
class Sort extends Component {

  onSort = (sortBy, sortValue) => {
    this.props.onSort(sortBy, sortValue);
  };

  render() {
    return (
      <MDBDropdown size="ls">
        <MDBDropdownToggle caret color="danger">
          Sắp xếp
        </MDBDropdownToggle>
        <MDBDropdownMenu color="danger" basic>
          <MDBDropdownItem onClick={() => this.onSort("name", 1)}>
            Tên A-Z{" "}
            <i
              className={
                this.props.sortBy === "name" && this.props.sortValue === 1
                  ? "fas fa-check-circle ml-5"
                  : ""
              }
            ></i>
          </MDBDropdownItem>
          <MDBDropdownItem onClick={() => this.onSort("name", -1)}>
            Tên Z-A{" "}
            <i
              className={
                this.props.sortBy === "name" && this.props.sortValue === -1
                  ? "fas fa-check-circle ml-5"
                  : ""
              }
            ></i>
          </MDBDropdownItem>
          <MDBDropdownItem onClick={() => this.onSort("status", 1)}>
            Trạng thái kích hoạt{" "}
            <i
              className={
                this.props.sortBy === "status" && this.props.sortValue === 1
                  ? "fas fa-check-circle ml-5"
                  : ""
              }
            ></i>
          </MDBDropdownItem>
          <MDBDropdownItem onClick={() => this.onSort("status", -1)}>
            Trạng thái ẩn{" "}
            <i
              className={
                this.props.sortBy === "status" && this.props.sortValue === -1
                  ? "fas fa-check-circle ml-5"
                  : ""
              }
            ></i>
          </MDBDropdownItem>
        </MDBDropdownMenu>
      </MDBDropdown>
    );
  }
}

export default Sort;
