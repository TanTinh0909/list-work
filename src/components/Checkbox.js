import React, { Component } from "react";
class CheckBox extends Component {
  render() {
    return (
      <div className="checkbox">
        <label className="custom-lable">
          <input
            key={this.props.id}
            onChange={this.props.handleCheckChieldElement}
            type="checkbox"
            checked={this.props.isChecked}
            value={this.props.value}
          />{" "}
          {this.props.value}
        </label>
      </div>
    );
  }
}

export default CheckBox;
